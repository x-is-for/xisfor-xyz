import lume from "lume/mod.ts";
import "lume/types.ts";

import date from "lume/plugins/date.ts";
import postcss from "lume/plugins/postcss.ts";
import terser from "lume/plugins/terser.ts";
import basePath from "lume/plugins/base_path.ts";
import slugifyUrls from "lume/plugins/slugify_urls.ts";
import resolveUrls from "lume/plugins/resolve_urls.ts";
import metas from "lume/plugins/metas.ts";
import pagefind from "lume/plugins/pagefind.ts";
import sitemap from "lume/plugins/sitemap.ts";
import feed from "lume/plugins/feed.ts";
import transformImages from "lume/plugins/transform_images.ts";
import markdownImage from "https://deno.land/x/lume_markdown_plugins@v0.7.1/image.ts";
import { alert } from "npm:@mdit/plugin-alert@0.13.1";

import { expandGlob } from "https://deno.land/std@0.224.0/fs/expand_glob.ts";

const site = lume({
  src: "src",
  location: new URL("https://xisfor.xyz/"),
});

site
  .use(postcss())
  .use(basePath())
  .use(date())
  .use(metas())
  .use(markdownImage())
  .use(resolveUrls())
  .use(slugifyUrls())
  .use(terser())
  .use(pagefind())
  .use(sitemap())
  .use(transformImages())
  .use(
    feed({
      output: ["/feed.xml", "/feed.json"],
      query: "type=book",
      info: {
        title: "=metas.site",
        description: "=metas.description",
      },
      items: {
        title: "=title",
      },
    })
  )
  .copy("favicon.png");

// Alert plugin
site.hooks.addMarkdownItPlugin(alert);
// Add default image, if it exists, mirroring page URL
site.preprocess([".md"], async (pages) => {
  const root = site.src();
  for (const page of pages) {
    let image = page.data.image;
    if (!image) {
      const glob = expandGlob(`images/${page.src.path}.*`, { root });
      const entry = await glob.next();
      image = entry.value?.path.slice(root.length);
      if (image) page.data.image = image;
    }
    // transformImages plugin will ensure the icon exists
    if (!page.data.icon && image) {
      page.data.icon = image.replace(/\.\w+?$/, "-icon.jpg");
    }
  }
});

export default site;
