---
title: About
---

Xylophone? Go home! X-ray? No way! From [xanthan](https://www.thefreedictionary.com/xanthan) to [xyst](https://www.thefreedictionary.com/xyst), there are so many more words to learn that start with the letter X!
