export const layout = "layouts/categories.vto";

export default function* ({ search }) {
  // Generate a page for each tag
  for (const tag of search.values("tags")) {
    yield {
      url: `/tag/${tag}/`,
      title: `Category: ${tag}`,
      type: "tag",
      search_query: `type=book '${tag}'`,
      tag,
    };
  }

  // Generate a page for each author
  for (const author of search.values("author")) {
    yield {
      url: `/author/${author}/`,
      title: `Books by ${author}`,
      type: "author",
      search_query: `type=book author*='${author}'`,
      author,
    };
  }

  // Generate a page for each illustrator
  for (const illustrator of search.values("illustrator")) {
    yield {
      url: `/illustrator/${illustrator}/`,
      title: `Books by ${illustrator}`,
      type: "illustrator",
      search_query: `type=book illustrator*='${illustrator}'`,
      illustrator,
    };
  }
}
