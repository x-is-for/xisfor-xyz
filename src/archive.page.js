export const layout = "layouts/archive.vto";
export const title = "Archive";

export default function* ({ search, paginate }) {
  const books = search.pages("type=book", "date=desc");

  for (const data of paginate(books, { url, size: 10 })) {
    yield data;
  }
}

function url(n) {
  if (n === 1) {
    return "/archive/";
  }

  return `/archive/${n}/`;
}
